﻿using System;

namespace ClassLib
{
   public class Loch
    {
        public string Name;
        public string Country;
        public string Region;
        public double Area;
        public double WaterVolume;
        public string Color;
        public bool HasWaterFall;

        public void CleanlinessOfTheLoch()
        {
            if (Country == "Україна" || Country == "Росія" || Country == "Білорусь")
            {
                Console.WriteLine("Озеро брудне");
            }
            else
                Console.Write("Озеро Чисте");

        }
        
        static public void Pause(string str)
        {
            Console.WriteLine(str);
            Console.ReadKey();
        }

        static public void Space()
        {
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
