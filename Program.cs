﻿using System;
using ClassLib;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            Loch[] arrLochs;

            Console.WriteLine("Введіть кількість озер:");
            int cntLochs = int.Parse(Console.ReadLine());
            arrLochs = new Loch[cntLochs];

            for (int i = 0; i < cntLochs; i++)
            {
                Console.Write("Введiть назву Озера: ");
                string sName = Console.ReadLine();

                Console.Write("Введiть назву країни где знаходиться озеро: ");
                string sCountry = Console.ReadLine();

                Console.Write("Введiть назву області где знаходиться озеро: ");
                string sRegion = Console.ReadLine();

                Console.Write("Введiть  площу бассейну: ");
                string sArea = Console.ReadLine();

                Console.Write("Введiть обсяг води: ");
                string sWaterVolume = Console.ReadLine();

                Console.Write("Введiть кольор води: ");
                string sColor = Console.ReadLine();

                Console.Write("Чи є біля озера водоспад? (у - так) (n - ні): ");
                ConsoleKeyInfo keyHasWaterFall = Console.ReadKey();

                Loch.Space();

                Loch theLoch = new Loch();
                theLoch.Name = sName;
                theLoch.Country = sCountry;
                theLoch.Region = sRegion;
                theLoch.Area = double.Parse(sArea);
                theLoch.WaterVolume = double.Parse(sWaterVolume);
                theLoch.Color = sColor;
                theLoch.HasWaterFall = keyHasWaterFall.Key == ConsoleKey.Y ? true : false;

                arrLochs[i] = theLoch;
            }

            foreach (Loch l in arrLochs)
            {
                Console.WriteLine();
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Данi про об`ект {0}", l.Name);
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Країна: " + l.Country);
                Console.WriteLine("Область: " + l.Region);
                Console.WriteLine("Площа бассейну: " + l.Area);
                Console.WriteLine("Обсяг воды: " + l.WaterVolume);
                Console.WriteLine("Кольор воды: " + l.Color);
                Console.WriteLine(l.HasWaterFall ? "Біля озера є водоспад" : "Біля озера немає водосподу");
            }
            Loch.Pause("Натисніть Будь-яку клавішу");
        }
    }
}
